#define F_CPU 8000000UL // Рабочая частота контроллера
#define BAUD 1952L // Скорость обмена данными
#define UBRRL_value (F_CPU/(BAUD*16UL))-1 //Согластно заданной скорости подсчитываем значение для регистра UBRR

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define LED_PORT	PORTB
#define LED_PIN		PB1
#define LEN 32

char buffer[LEN];
volatile unsigned char done;
volatile unsigned char IDX;

void uv_on();
void uv_off();

void led_on()
{
	LED_PORT |= (1 << LED_PIN);
}

void led_off()
{
	LED_PORT &= ~(1 << LED_PIN);
}

void uart_init( void )
{
	UBRRH=0x00;
	UBRRL=0x33;
	
	//разрешить прием и передачу данных
	UCSRB = ( 1 << TXEN ) | ( 1 << RXEN ) | (1 << RXCIE);

	//8 бит данных, 1 стоп бит, без контроля четности
	UCSRC = ( 1 << URSEL ) | ( 1 << UCSZ1 ) | ( 1 << UCSZ0 );

}

void uart_putc( char c )
{
	//ждем окончания передачи предыдущего байта
	while( ( UCSRA & ( 1 << UDRE ) ) == 0  );
	//передача данных
	UDR = c;
}

void uart_puts( char *str )
{
	unsigned char c;
	while( ( c = *str++ ) != 0 ) {
		uart_putc( c );
	}
}

// обработка прерывания
ISR(USART_RXC_vect)
{
	char bf= UDR;
	buffer[IDX]=bf;
	IDX++;

	if (bf == ':' || bf == '.' || IDX >= LEN)
	{
		IDX=0;
		done=1;
		if (bf == ':') {
			led_on();	
		} else if (bf == '.') {
			led_off();	
		}
		
	}
}

int main(void)
{
	DDRB = 0b11111111; // set pin
	PORTB = 0x00;
	
	led_off();
	
	_delay_ms(100);
	uart_init();
	
	led_on();
	uart_puts("hello: smart home");
	_delay_ms(100);
	led_off();

	IDX=0;

	sei();
	
	while( 1 ) {
		
	
	}
}
